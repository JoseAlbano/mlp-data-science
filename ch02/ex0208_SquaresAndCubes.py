''' Chapter 2: Introduction to Python Programming '''
# Exercise 2.8: Table of Squares and Cubes

print('*** Exercise 2.8: Table of Squares and Cubes ***\n')

print('number', '\tsquare', '\tcube')
number = 0
print(number, '\t' + str(number ** 2), '\t' + str(number ** 3))

number = number + 1
print(number, '\t' + str(number ** 2), '\t' + str(number ** 3))

number = number + 1
print(number, '\t' + str(number ** 2), '\t' + str(number ** 3))

number = number + 1
print(number, '\t' + str(number ** 2), '\t' + str(number ** 3))

number = number + 1
print(number, '\t' + str(number ** 2), '\t' + str(number ** 3))

number = number + 1
print(number, '\t' + str(number ** 2), '\t' + str(number ** 3))