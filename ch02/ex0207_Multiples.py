''' Chapter 2: Introduction to Python Programming '''
# Exercise 2.7: Multiples

print('*** Exercise 2.7: Multiples ***\n')

if 1024 % 4 == 0:
  print('1024 is Multiple of 4.')
else:
  print('1024 is not Multiple of 4.')

if 2 % 10 == 0:
  print('2 is Multiple of 10.')
else:
  print('2 is not Multiple of 10.')