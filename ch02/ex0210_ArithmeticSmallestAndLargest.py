''' Chapter 2: Introduction to Python Programming '''
# Exercise 2.10: Arithmetic, Smallest and Largest

print('*** Exercise 2.10: Arithmetic, Smallest and Largest ***\n')

number1 = int( input('\tEnter the First Number: ') )
number2 = int( input('\tEnter the Second Number: ') )
number3 = int( input('\tEnter the Third Number: ') )

print()

print('\t\tNumbers:', number1, number2, number3, '\n')

print( '\t\tSum:\t', number1 + number2 + number3)
print( '\t\tAverage:', (number1 + number2 + number3)/3 )
print( '\t\tProduct:', number1 * number2 * number3)
print( '\t\tSmallest:', min(number1, number2, number3))
print( '\t\tLargest:', max(number1, number2, number3))