''' Chapter 2: Introduction to Python Programming '''
# Exercise 2.9: Integer Value of a Character

print('*** Exercise 2.9: Integer Value of a Character ***\n')

print( '\tB:\t', ord('B'))
print( '\tC:\t', ord('C'))
print( '\tD:\t', ord('D'))
print()

print( '\tb:\t', ord('b'))
print( '\tc:\t', ord('c'))
print( '\td:\t', ord('d'))
print()

print( '\t0:\t', ord('0'))
print( '\t1:\t', ord('1'))
print( '\t2:\t', ord('2'))
print()

print( '\t$:\t', ord('$'))
print( '\t"Space":', ord(' '))