''' Chapter 2: Introduction to Python Programming '''
# Exercise 2.15 Sort in Ascending Order

print('*** Exercise 2.15 Sort in Ascending Order ***\n')

number1 = float( input('\tEnter the First Number: ') )
number2 = float( input('\tEnter the Second Number: ') )
number3 = float( input('\tEnter the Third Number: ') )

aux = 0

if number1 > number2:
  aux = number1
  number1 = number2
  number2 = aux

  if number2 > number3:
    aux = number2
    number2 = number3
    number3 = aux
  
if number2 > number3:
    aux = number2
    number2 = number3
    number3 = aux

    if number1 > number2:
      aux = number1
      number1 = number2
      number2 = aux

if number1 > number2:
  aux = number1
  number1 = number2
  number2 = aux

print('\n\t', number1, number2, number3)