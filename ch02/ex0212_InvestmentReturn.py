''' Chapter 2: Introduction to Python Programming '''
# Exercise 2.12 7% Investment Return

print('*** Exercise 2.12 7% Investment Return ***\n')


originalAmount = float( input('\tOriginal Amount: ') )
anualRate = float( input('\tAnnual Rate (%): ') ) / 100
years = int( input('\tYears: ') )


finalAmount = originalAmount * (1 + anualRate ) ** years


print( '\n\tFinal Amount', finalAmount, 'in', years, 'years.' )