''' Chapter 2: Introduction to Python Programming '''
# Exercise 2.14 Target Heart-Rate Calculator

print('*** Exercise 2.14 Target Heart-Rate Calculator ***\n')


age = int( input('\tEnter your Age: '))

maxHeartRate = 220 - age
minTargetHeartRage = maxHeartRate * .5
maxTargetHeartRage = maxHeartRate * .85

print( '\n\tYour Maximum Heart Rate:', maxHeartRate )
print( '\tYour Target Heart Rate Range:', str(minTargetHeartRage) + '-' + \
  str(maxTargetHeartRage) )