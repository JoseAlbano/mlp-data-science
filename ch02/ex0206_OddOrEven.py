''' Chapter 2: Introduction to Python Programming '''
# Exercise 2.6: Circle Area, Diameter and Circumference

print('*** Exercise 2.6: Odd or Even ***\n')

number = int(input('Enter an Integer number: '))

print()

if (number % 2 == 0):
  print(number, "is Even.")
else:
  print(number, "is Odd.")