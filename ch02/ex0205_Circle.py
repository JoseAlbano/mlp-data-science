''' Chapter 2: Introduction to Python Programming '''
# Exercise 2.5: Circle Area, Diameter and Circumference

print('*** Exercise 2.5: Circle Area, Diameter and Circumference ***\n')

r = 2
PI = 3.14159

diameter = 2 * r
circumference = 2 * PI * r
area = PI * ( r ** 2 )

print('Circle Radius:', r, '\n')
print('Diameter:', diameter)
print('Circumference:', circumference)
print('Area:', area)