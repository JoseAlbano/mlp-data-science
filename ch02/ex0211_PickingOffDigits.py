''' Chapter 2: Introduction to Python Programming '''
# Exercise 2.11: Separating the Digits in an Integer

print('*** Exercise 2.11: Separating the Digits in an Integer ***\n')

number = int( input('\tEnter the an Integer Number of five digits: ') )

print()

print('\t', \
  number % 100000 // 10000, \
  number % 10000 // 1000, \
  number % 1000 // 100, \
  number % 100 // 10, \
  number % 10, '\n')