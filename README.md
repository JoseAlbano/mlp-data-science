# My Learning Path: Data Analysis to Data Science

## Book: *Intro to Python for Computer Science and Data Science* by Paul Deitel and Harvey Deitel

Hello.

Here are my Solutions to the Exercises in the Book.

### My Progress


#### Computer Science path

- [x] Chapter 01: Introduction to Computers and Python
- [ ] Chapter 02: Introduction to Python Programming
- [ ] Chapter 03: Control Statements and Program Development
- [ ] Chapter 04: Functions
- [ ] Chapter 05: Lists and Tuples
- [ ] Chapter 06: Dictionaries and Sets
- [ ] Chapter 07: Array-Oriented Programming with NumPy
- [ ] Chapter 08: Strings: A Deeper Look
- [ ] Chapter 09: Files and Exceptions
- [ ] Chapter 10: Object-Oriented Programming
- [ ] Chapter 11: Computer Science Thinking: Recursion, Searching, Sorting and Big O 


#### Data Science path

- [x] Chapter 01 - DS Intro: AI—at the Intersection of CS and DS
- [ ] Chapter 02 - DS Intro: Basic Descriptive Stats
- [ ] Chapter 03 - DS Intro: Measures of Central Tendency—Mean, Median, Mode
- [ ] Chapter 04- DS Intro: Basic Statistics—Measures of Dispersion
- [ ] Chapter 05 - DS Intro: Simulation and Static Visualization
- [ ] Chapter 06 - DS Intro: Simulation and Dynamic Visualization
- [ ] Chapter 07 - DS Intro: Pandas Series and DataFrames
- [ ] Chapter 08 - DS Intro: Pandas, Regular Expressions and Data Wrangling
- [ ] Chapter 09 - DS Intro: Loading Datasets from CSV Files into Pandas DataFrames
- [ ] Chapter 10 - DS Intro: Time Series and Simple Linear Regression
- [ ] Chapter 12: Natural Language Processing (NLP)
- [ ] Chapter 13: Data Mining Twitter
- [ ] Chapter 14: IBM Watson and Cognitive Computing
- [ ] Chapter 15: Machine Learning: Classification, Regression and Clustering
- [ ] Chapter 16: Deep Learning
- [ ] Chapter 17: Big Data: Hadoop, Spark, NoSQL and IoT
